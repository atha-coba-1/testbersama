const inputBox = document.getElementById("input-box");
const timeBox = document.getElementById("time-box");
const listContainer = document.getElementById("list-container");

function addTask() {
    if(inputBox.value === ''){
        alert("You must write something!");
    } else if(timeBox.value === '') {
        alert("You must set a time!");
    } else {
        let li = document.createElement("li");
        li.innerHTML = inputBox.value;
        
        // Mengambil nilai waktu dari input time
        let time = document.createElement("span");
        time.classList.add("time");
        time.innerHTML = timeBox.value;
        li.appendChild(time);
        
        let span = document.createElement("span");
        span.innerHTML = "\u00d7";
        li.appendChild(span);
        
        listContainer.appendChild(li);
    }
    inputBox.value = "";
    timeBox.value = ""; // Reset input time setelah menambahkan task
    saveData();
}

listContainer.addEventListener("click", function(e) {
    if(e.target.tagName === "LI") {
        e.target.classList.toggle("checked");
        saveData();
    } else if(e.target.tagName === "SPAN" && e.target.className !== "time") {
        e.target.parentElement.remove();
        saveData();
    }
}, false);

function saveData() {
    localStorage.setItem("data", listContainer.innerHTML);
}

function showTask() {
    listContainer.innerHTML = localStorage.getItem("data");
}

showTask();
